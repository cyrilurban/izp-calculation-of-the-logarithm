/**
 * @file    proj2.c
 * @author  CYRIL URBAN
 * @date:   2015-11-20
 * @brief   Vypocet prirozeneho logaritmu
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <float.h>

// funkce na vypocet prirozeneho logaritmu zapomoci Tayl. rady
double taylor_log(double x, unsigned int n)
{
	if (x == 0) // kdyz bude x == 0 , tak vratim -INF
	{
		return (-INFINITY);
	}
	else if ( x < 0) // kdyz bude x < 0 , tak vratim NAN
	{
		return (NAN);
	}

	else if (x >= 1) // reseni log pro x >= 1
	{
		double zaklad = (x - 1) / x;
		double citatel = 1;
		double jmenovatel = 0.0;
		double krok = 1.0;
		double vysledek = 0.0;

		while (jmenovatel < n)
		{
			citatel = citatel * zaklad;
			jmenovatel = jmenovatel + 1.0;
			krok = citatel / jmenovatel;
			vysledek = vysledek + krok;
		}

		return vysledek;
	}
	else // reseni log pro x < 1
	{
		double zaklad = (1 - x);
		double citatel = 1;
		double jmenovatel = 0.0;
		double krok = 1.0;
		double vysledek = 0.0;

		while (jmenovatel < n)
		{
			citatel = citatel * zaklad;
			jmenovatel = jmenovatel + 1.0;
			krok = citatel / jmenovatel;
			vysledek = vysledek - krok;
		}
		return vysledek;
	}
}

double cfrac_log(double x, unsigned int n)
{
	if (x == 0)
	{
		return (-INFINITY);
	}
	else if ( x < 0)
	{
		return (NAN);
	}

	else
	{
		double cf = 0;
		double a;
		double b;
		double z = (x - 1) / (x + 1); // vypocitani zakladu

		while (n > 0)
		{
			a = z * z * n * n; // citatel (1.clen: z^2)
			b = (n * 2) + 1; // 1. cast jmenovatele (licha cisla)
			cf = a / (b - cf); // citatel "lomeno" (liche cislo "minus" predesly zlomek (1. byl 0))
			n--;
		}
		return (2 * z) / (1 - cf); // vracim
	}
}

double ABS(double x) // funkce na absolutni hodnotu
{
	if (x < 0)
	{
		x = x * (-1.0);
	}
	return x;
}

void iter(double min, double max, double eps)
{
	printf("log(%.5g) = %.12g\n", min, log (min));
	printf("log(%.5g) = %.12g\n", max, log (max));

	int c_min = 1;
	double presnost2min = log(min);
	double presnost2max = log(max);

	while (min > 0) // hledani presnosti pro cf_log MIN
	{
		double presnost1 = cfrac_log(min, c_min);
		double presnost = ABS(presnost2min - presnost1); // porovnavam vysledek logaritmu s vysledkem knihovniho logaritmu

		if (presnost <= eps)
		{
			break;
		}
		c_min++;
	}

	int c_max = 1;
	while (max > 0) // hledani presnosti pro cf_log MAX
	{
		double presnost1 = cfrac_log(max, c_max);
		double presnost = ABS(presnost2max - presnost1); // porovnavam vysledek logaritmu s vysledkem knihovniho logaritmu

		if (presnost <= eps)
		{
			break;
		}
		c_max++;
	}

	if (c_min < c_max) // vetsi pocet iteraci je rozhodujici
	{
		c_min = c_max;
	}

	int t_min = 1;
	while (min > 0) // hledani presnosti pro taylor MIN
	{
		double presnost1 = taylor_log(min, t_min);
		double presnost = ABS(presnost2min - presnost1); // porovnavam vysledek logaritmu s vysledkem knihovniho logaritmu

		if (presnost <= eps)
		{
			break;
		}
		t_min++;
	}

	int t_max = 1;
	while (max > 0) // hledani presnosti pro taylor MAX
	{
		double presnost1 = taylor_log(max, t_max);
		double presnost = ABS(presnost2max - presnost1); // porovnavam vysledek logaritmu s vysledkem knihovniho logaritmu

		if (presnost <= eps)
		{
			break;
		}
		t_max++;
	}

	if (t_min < t_max) // vetsi pocet iteraci je rozhodujici
	{
		t_min = t_max;
	}

	if ((min <= 0) && (max <= 0))
	{
		c_min = 0;
		t_min = 0;
	}

	printf("continued fraction iterations = %d\n", c_min + 1); // cfrac_log mi provede nulty krok, proto + 1

	double cfrac_min = cfrac_log(min, c_min);
	printf("cf_log(%.5g) = %.12g\n", min, cfrac_min);

	double cfrac_max = cfrac_log(max, c_min);
	printf("cf_log(%.5g) = %.12g\n", max, cfrac_max);

	printf("taylor polynomial iterations = %d\n", t_min);

	double taylor_min = taylor_log(min, t_min);
	printf("taylor_log(%.5g) = %.12g\n", min, taylor_min);

	double taylor_max = taylor_log(max, t_min);
	printf("taylor_log(%.5g) = %.12g\n", max, taylor_max);
}

int main(int argc, char *argv[])
{
	char *pismeno = '\0';
	char *pismeno2 = '\0';
	char *pismeno3 = '\0';

	// cast programu pro "--log" (brano z 1. argumentu)
	if ((argc == 4) && (strcmp(argv[1], "--log") == 0))
	{
		double x = strtod(argv[2], &pismeno);
		unsigned int n = strtod(argv[3], &pismeno2);

		if ((*pismeno != '\0') || (*pismeno2 != '\0'))
		{
			fprintf(stderr, "Error! Invalid arguments\n"); // chybova hlaska - spatne argumenty - bylo zadano pismeno misto cisla
		}
		else if (n < 1)
		{
			fprintf(stderr, "Error! N must be bigger then 0 \n"); // chybova hlaska - nulovy nebo zaporny pocet iteraci
		}
		else if ((x > DBL_MAX || n > UINT_MAX))
		{
			fprintf(stderr, "Error, memory overflow\n"); // chybova hlaska - preteceni
		}
		else
		{
			printf("	   log(%.5g) = %.12g\n", x, log (x));

			double taylor = taylor_log(x, n);
			double cfrac = cfrac_log(x, n);

			printf("	cf_log(%.5g) = %.12g\n", x, cfrac);
			printf("taylor_log(%.5g) = %.12g\n", x, taylor);
		}
	}
	// cast programu pro "--iter" (brano z 1. argumentu)
	else if ((argc == 5) && (strcmp(argv[1], "--iter") == 0))
	{
		double min = strtod(argv[2], &pismeno);
		double max = strtod(argv[3], &pismeno2);
		double eps = strtod(argv[4], &pismeno3);

		if ((*pismeno != '\0') || (*pismeno2 != '\0') || (*pismeno3 != '\0'))
		{
			fprintf(stderr, "Error! Invalid arguments\n"); // chybova hlaska - spatne argumenty - bylo zadano pismeno misto cisla
		}
		else if (eps <= 0)
		{
			fprintf(stderr, "Error! EPS must be bigger then 0 \n"); // chybova hlaska - presnost musi byt vetsi, nez 0
		}
		else if (min > DBL_MAX || max > DBL_MAX)
		{
			fprintf(stderr, "Error, memory overflow\n"); // chybova hlaska - preteceni
		}
		else
		{
			iter(min, max, eps);
		}
	}
	else
	{
		fprintf(stderr, "Error! Invalid arguments\n"); // chybova hlaska - spatne argumenty
	}

	return 0;
}
